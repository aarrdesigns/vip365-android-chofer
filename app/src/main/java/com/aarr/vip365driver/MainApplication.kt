package com.aarr.vip365driver

import android.app.Application
import android.content.Context

/**
 * Created by andresrodriguez on 10/23/17.
 */
class MainApplication : Application() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
//        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        MainApplication.appContext = applicationContext
    }

    companion object {
        var appContext: Context? = null
            private set
    }

}
