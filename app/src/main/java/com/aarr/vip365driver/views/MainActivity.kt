package com.aarr.vip365driver.views

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.widget.Button
import android.view.animation.AnticipateOvershootInterpolator
import android.transition.Explode
import android.view.View

import com.aarr.vip365driver.R

internal class MainActivity : AppCompatActivity() {

    var btnIniciar: Button? = null
    var btnRegistrar: Button? = null
    var REQUEST_CODE = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnIniciar = findViewById(R.id.btnIniciar)
        btnIniciar!!.setOnClickListener {
            goIniciar()
        }
        btnRegistrar = findViewById(R.id.btnRegistrar)
        btnRegistrar!!.setOnClickListener {
//            goRegistro()
        }

    }

    fun goIniciar() {
        var intent: Intent? = Intent(this, LoginActivity::class.java)
        startActivityForResult(intent, REQUEST_CODE)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun makeExplodeTransition(): Explode {
        val explode = Explode()
        explode.duration = 3000
        explode.interpolator = AnticipateOvershootInterpolator()
        return explode
    }

    // Custom method to toggle visibility of views
    private fun toggleVisibility(vararg views: View) {
        // Loop through the views
        for (v in views) {
            if (v.getVisibility() === View.VISIBLE) {
                v.setVisibility(View.INVISIBLE)
            } else {
                v.setVisibility(View.VISIBLE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                finish()
                var intent = Intent(this, MenuActivity::class.java)
                startActivity(intent)
            }
        }
    }
}
