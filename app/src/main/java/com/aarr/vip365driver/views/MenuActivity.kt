package com.aarr.vip365driver.views

import android.Manifest
import android.animation.Animator
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.media.ExifInterface
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewAnimationUtils
import android.view.animation.LinearInterpolator
import android.widget.*
import com.aarr.vip365driver.database.Dao.UsersDao
import com.aarr.vip365driver.database.Model.TripsModel
import com.aarr.vip365driver.database.Model.UsersModel

import com.aarr.vip365driver.R
import com.aarr.vip365driver.services.PlaceDetails.Result
import com.aarr.vip365driver.services.PlaceDetailsApi
import com.aarr.vip365driver.services.PlaceDirections.Leg
import com.aarr.vip365driver.services.PlaceDirections.PlacesDirections
import com.aarr.vip365driver.services.PlaceDirections.Route
import com.aarr.vip365driver.services.PlaceDirections.Step
import com.aarr.vip365driver.services.PlaceGeocodeApi
import com.aarr.vip365driver.services.PlaceGeocodeApi.DESTINO
import com.aarr.vip365driver.services.PlaceGeocodeApi.ORIGEN
import com.aarr.vip365driver.services.PlacesDirectionsApi
import com.aarr.vip365driver.services.TripService
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class MenuActivity : AppCompatActivity(), SensorEventListener, NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private var mMap: GoogleMap? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var locationSearch: AutoCompleteTextView? = null
    private var reciclador: RecyclerView? = null
    private var currentLocation: Location? = null
    private var placeId: String? = null
    private var currentResult: Result? = null
    private val EXTRA_USER = "EXTRA_USER"
    private val EXTRA_TYPE = "EXTRA_TYPE"
    private var myMarker: Marker? = null
    private var isMarkerRotating = false
    private var sensorMan: SensorManager? = null
    var mGravity: FloatArray? = null
    var mMagnetic: FloatArray? = null
    var compass_last_measured_bearing: Float = 0F
    var currentUser: UsersModel? = null
    var txtNombre: TextView? = null
    var txtCalificacion: TextView? = null
    var imgProfile: ImageView? = null
    var navigationView: NavigationView? = null
    var revealContainer: RelativeLayout? = null
    private var accelerometer: Sensor? = null
    private var mAccel: Float = 0.toFloat()
    private var mAccelCurrent: Float = 0.toFloat()
    private var mAccelLast: Float = 0.toFloat()
    var REQUEST_CODE = 100
    var mapFragment: SupportMapFragment? = null
    var btnTest: FloatingActionButton? = null
    var btnAlert: FloatingActionButton? = null
    private var destino: String? = null
    private var origen: String? = null
    private var polyline = PolylineOptions()
    private var marker: Marker? = null
    private var lastBearing = 0.0f
    private var lastLatLng: LatLng? = null
    private var lastLocation: Location? = null
    var selectLocation: ImageView? = null
    var btnFinish: Button? = null
    var myLocation: LatLng? = null
    private var poly: Polyline? = null
    private var txtDirections: TextView? = null
    private var directionsContainer: RelativeLayout? = null
    var isMapSelection = false
    var toolbar: Toolbar? = null
    var fab: FloatingActionButton? = null
    var drawer: DrawerLayout? = null

    private val type: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        toolbar = findViewById(R.id.toolbar)
        toolbar!!.setTitle(" ")
        setSupportActionBar(toolbar)

        btnTest = findViewById(R.id.btnTest)
        btnTest!!.setOnClickListener {
            selectLocationInMap(0)
        }

        directionsContainer = findViewById(R.id.directionsContainer)
        txtDirections = findViewById(R.id.txtDirections)

        btnFinish = findViewById(R.id.btnFinish)
        btnFinish!!.setOnClickListener {
            val placesDirectionsApi = PlacesDirectionsApi(this@MenuActivity)
            placesDirectionsApi.getPlaceDirections(origen, destino)
        }

        selectLocation = findViewById(R.id.selectLocation)

        btnAlert = findViewById(R.id.btnAlert)
        btnAlert!!.setOnClickListener {
            var user = UsersDao().getCurrentUser()
            var myLocation = currentLocation!!.latitude.toString()+","+currentLocation!!.longitude
            var trip = TripsModel(
                    -1,
                    -1,
                    user!!.idServer,
                    -1,
                    Date(),
                    Date(),
                    myLocation,
                    myLocation,
                    "Mi Ubicacion",
                    "Ubicacion de destino",
                    1,
                    myLocation,
                    0.0,
                    3,
                    -1,
                    false
            )
            TripService(this).registerTrip(trip)
        }

//        locationSearch = findViewById(R.id.locationSearch) as AutoCompleteTextView
//        locationSearch!!.setOnClickListener {
//            var intent = Intent(this,BuscarRutaActivity::class.java)
//            startActivity(intent)
//        }

        revealContainer = findViewById(R.id.revealContainer)


        fab = findViewById(R.id.fab)
        fab!!.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        drawer = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer!!.setDrawerListener(toggle)
        toggle.syncState()

        navigationView = findViewById(R.id.nav_view)
        navigationView!!.setNavigationItemSelectedListener(this)
        setUserData()

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build()
            mGoogleApiClient!!.connect()
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        try{
            mapFragment = supportFragmentManager!!
                    .findFragmentById(R.id.mapView) as SupportMapFragment
            mapFragment!!.getMapAsync(this)
        }catch (e:Exception){
            Log.w("Exception",e.toString())
        }

        initSensors()

    }

    fun setUserData(){
        var header = navigationView!!.getHeaderView(0)
        txtNombre = header.findViewById(R.id.txtNombre)
        txtCalificacion = header.findViewById(R.id.txtCalificacion)
        imgProfile = header.findViewById(R.id.imgProfile)

        currentUser = UsersDao().getCurrentUser()
        if (currentUser!=null){
            txtNombre!!.setText(currentUser!!.name+" "+currentUser!!.lastName)
//            txtCalificacion!!.setText(currentUser!!.name+" "+currentUser!!.lastName)
            if (currentUser!!.profileImage!=null && !currentUser!!.profileImage.equals("")){

                var imageUri = Uri.parse(File(currentUser!!.profileImage).toString())

                val exif = ExifInterface(imageUri.getPath())
                val rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
                Log.e("ROTATIONVALUE",rotation.toString())

                val options = RequestOptions()
                options.diskCacheStrategy(DiskCacheStrategy.NONE)
                options.skipMemoryCache(true)
                options.fitCenter()
                options.dontTransform()
                options.dontAnimate()
                Glide
                        .with(this)
                        .load(currentUser!!.profileImage)
                        .apply(options)
                        .into(imgProfile)
            }
        }
    }

    fun initSensors() {
        sensorMan = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accelerometer = sensorMan!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        mAccel = 0.00f
        mAccelCurrent = SensorManager.GRAVITY_EARTH
        mAccelLast = SensorManager.GRAVITY_EARTH
    }

    override fun onBackPressed() {
        drawer = findViewById(R.id.drawer_layout)
        if (drawer!!.isDrawerOpen(GravityCompat.START)) {
            drawer!!.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if (id == R.id.pago_item) {
//            showPagos()
        } else if (id == R.id.viajes_item) {

        } else if (id == R.id.ayuda_item) {

        } else if (id == R.id.config_item) {
//            showConfiguration()
        }
//        else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        drawer = findViewById(R.id.drawer_layout)
        drawer!!.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onStart() {
        super.onStart()
//        if (mGoogleApiClient!=null){
//            mGoogleApiClient!!.connect()
//        }
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mGoogleApiClient!=null){
            mGoogleApiClient!!.disconnect()
        }
    }

    override fun onMapReady(map: GoogleMap?) {
        mMap = map
        mMap!!.uiSettings.isCompassEnabled = false
//        mMap!!.isMyLocationEnabled = true
//        var locationButton = ( mapFragment!!.view!!.findViewById(Integer.parseInt("1")).getParent() as View).findViewById(Integer.parseInt("2")) as ImageView
////        locationButton.setBackgroundResource(R.drawable.circle_bg_black_border)
////        locationButton.setPadding(3,3,3,3)
//        var rlp =  locationButton!!.getLayoutParams() as RelativeLayout.LayoutParams
//
//        // position on right bottom
//        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
//        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
//        rlp.setMargins(0, 0, 30, 30)
//        mMap!!.setOnMyLocationButtonClickListener(object: GoogleMap.OnMyLocationButtonClickListener{
//            override fun onMyLocationButtonClick(): Boolean {
//
//                return false
//            }
//        })


    }

    fun getLocation(init:Boolean?): LatLng? {
        var newLocation: LatLng? = null
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null
        }
        val mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient)
        if (mLastLocation != null) {
            currentLocation = mLastLocation
            Log.v("GooglePlacesApi", mLastLocation.toString())
            newLocation = LatLng(mLastLocation.latitude,mLastLocation.longitude)
            val icon = BitmapFactory.decodeResource(this@MenuActivity.getResources(),
                    R.mipmap.pin_user_24)
//            if (myMarker==null){
//                myMarker = mMap!!.addMarker(MarkerOptions()
//                        .position(newLocation!!)
//                        .icon(BitmapDescriptorFactory.fromBitmap(icon))
//                        .flat(true))
//            }else{
//                animateMarker(myMarker!!,newLocation,false,true)
//            }
//            mMap!!.addMarker(MarkerOptions().position(newLocation!!))
            if (init!=null){
                if (!init){
                    mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(newLocation, 16.0f))
                }else{
                    mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(newLocation, 22.0f))
                }
            }

        }else{
            Log.v("GooglePlacesApi", "Location Is Null")
        }
        myLocation = newLocation
        return newLocation
    }



    fun showPagos(){
//        var intent = Intent(this,PagosActivity::class.java)
//        startActivity(intent)
    }

    fun showConfiguration(){
//        var intent = Intent(this,ConfiguracionActivity::class.java)
//        startActivityForResult(intent,REQUEST_CODE)
    }

//    fun fillAndResetRecycler(lista: List<Prediction>?) {
//        if (lista != null) {
//            reciclador.setAdapter(null)
//            val adapter = PredictionsAdapter(lista, this)
//            reciclador.setLayoutManager(LinearLayoutManager(this))
//            reciclador.setAdapter(adapter)
//        } else {
//            Toast.makeText(this, "Error al recuperar resultados, intente de nuevo", Toast.LENGTH_LONG).show()
//        }
//    }

//    fun initBuscador() {
//        val adapter = PlacesAutoCompleteAdapter(this, R.layout.autocomplete_list_item, currentLocation)
//        locationSearch!!.setAdapter(adapter)
//        locationSearch!!.setOnItemClickListener(AdapterView.OnItemClickListener { parent, view, position, id ->
//            val description = adapter.getPredictionItem(position)
//            //                Toast.makeText(RastreoPedidoActivity.this, description.getPlaceId(), Toast.LENGTH_SHORT).show();
//            Log.v("PlaceId", description.getPlaceId())
//            val placeDetailsApi = PlaceDetailsApi(this@MenuActivity, PlaceDetailsApi.ORIGEN)
//            placeDetailsApi.getPlaceDetails(description.getPlaceId())
//        })
//    }

    fun animateMarker(marker: Marker, toPosition: LatLng,
                      hideMarker: Boolean, isMyLocation:Boolean) {
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val proj = mMap!!.getProjection()
        val startPoint = proj.toScreenLocation(marker.position)
        val startLatLng = proj.fromScreenLocation(startPoint)
        val duration: Long = 500


        val interpolator = LinearInterpolator()

        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                val lng = t * toPosition.longitude + (1 - t) * startLatLng.longitude
                val lat = t * toPosition.latitude + (1 - t) * startLatLng.latitude
                val newPosition = LatLng(lat, lng)
                marker.setPosition(newPosition)
//                if (!isMyLocation){
//                    polyline.add(newPosition)
//                    polyline.width(10f)
//                    polyline.color(Color.BLUE)
//                    mMap.addPolyline(polyline)
//                }

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                } else {
                    if (hideMarker) {
                        marker.isVisible = false
                    } else {
                        marker.isVisible = true
                    }
                }
            }
        })
    }

    fun animateMarker(marker: Marker, toPosition: LatLng, fromPosition: LatLng, hideMarker: Boolean) {
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val proj = mMap!!.getProjection()
        val startPoint = proj.toScreenLocation(marker.position)
        val startLatLng = proj.fromScreenLocation(startPoint)
        val duration: Long = 500

        val interpolator = LinearInterpolator()
        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                val lng = t * toPosition.longitude + (1 - t) * startLatLng.longitude
                val lat = t * toPosition.latitude + (1 - t) * startLatLng.latitude
                val newPosition = LatLng(lat, lng)
                marker.setPosition(newPosition)
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                } else {
                    if (hideMarker) {
                        marker.isVisible = false
                    } else {
                        marker.isVisible = true
                    }
                }
            }
        })
        if (this.lastLatLng != null) {
            this.lastBearing = bearingBetweenLocations(toPosition, fromPosition).toFloat()
            rotateMarker(marker, this.lastBearing)
        }
    }

    fun getDetails(placeId: String) {
        if (placeId != "") {
            this.placeId = placeId
            Log.v("PlaceId", placeId)
            val placeDetailsApi = PlaceDetailsApi(this, PlaceDetailsApi.DIRECCIONES)
            placeDetailsApi.getPlaceDetails(placeId)
        }
    }

    fun setLocation(result: Result?, estado: Int) {
        val direccion = ""
        when (estado) {
            PlaceDetailsApi.ORIGEN -> if (result != null) {
                currentResult = result
                Log.v("MapPlaceLat", result!!.getGeometry().getLocation().getLat().toString())
                Log.v("MapPlaceLng", result!!.getGeometry().getLocation().getLng().toString())
                val lat = result!!.getGeometry().getLocation().getLat()
                val lng = result!!.getGeometry().getLocation().getLng()

                val location = Location(LocationManager.GPS_PROVIDER)
                location.latitude = lat.toDouble()
                location.longitude = lng.toDouble()
                Log.v("CurrentLocation", lat.toString() + "," + lng.toString())
                if (location != null) {
                    currentLocation = location
                }
                val newPosition = LatLng(lat.toDouble(), lng.toDouble())
//                origen = lat.toString() + "," + lng.toString()
//                mapPin.setVisibility(View.VISIBLE)
                mMap!!.clear()
                mMap!!.addMarker(MarkerOptions().position(newPosition).title(direccion))
                mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(newPosition, 16.0f))
            }
        }
    }

    override fun onLocationChanged(location: Location?) {

    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    override fun onProviderEnabled(provider: String?) {

    }

    override fun onProviderDisabled(provider: String?) {

    }

    override fun onPause() {
        super.onPause()
        sensorMan!!.unregisterListener(this)
    }

    override fun onConnected(p0: Bundle?) {
        Log.v("GooglePlacesApi", "Connected")
//        locationSearch!!.setEnabled(true)
//        locationSearch!!.requestFocus()
        val newLoc = getLocation(true)
        myLocation = newLoc
        origen = newLoc!!.latitude.toString()+","+newLoc!!.longitude.toString()
        val iconCar = BitmapFactory.decodeResource(this@MenuActivity.getResources(),
                R.mipmap.car_small)
        marker = mMap!!.addMarker(MarkerOptions()
                .position(newLoc!!)
                .icon(BitmapDescriptorFactory.fromBitmap(iconCar))
                .flat(true))
        try{
            revealMap(newLoc!!)
        }catch (e:Exception){
            Toast.makeText(this,"Reveal: "+e.toString(), Toast.LENGTH_LONG).show()
            Log.v("RevealException", e.toString())
        }
//        initBuscador()
    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }

    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            mGravity = event.values.clone()
            // Shake detection
            val x = mGravity!![0]
            val y = mGravity!![1]
            val z = mGravity!![2]
            mAccelLast = mAccelCurrent
            mAccelCurrent = Math.sqrt((x * x + y * y + z * z).toDouble()).toFloat()
            val delta = mAccelCurrent - mAccelLast
            mAccel = mAccel * 0.9f + delta
            // Make this higher or lower according to how much
            // motion you want to detect
            if (mAccel > 10) {
                Log.v("Location", "Acceleration: " + mAccel + "")
//                Toast.makeText(this, "Moving...", Toast.LENGTH_SHORT).show()
                var newLoc = getLocation(null)
                if (newLoc!=null){
                    origen = newLoc!!.latitude.toString()+","+newLoc.longitude.toString()
                    val placesDirectionsApi = PlacesDirectionsApi(this@MenuActivity)
                    placesDirectionsApi.getPlaceDirections(origen, destino)
                }
            }
        }

    }

    private fun rotateMarker(marker: Marker, toRotation: Float) {
        if (!isMarkerRotating) {
            val handler = Handler()
            val start = SystemClock.uptimeMillis()
            val startRotation = marker.rotation
            val duration: Long = 2000

            val interpolator = LinearInterpolator()

            handler.post(object : Runnable {
                override fun run() {
                    isMarkerRotating = true

                    val elapsed = SystemClock.uptimeMillis() - start
                    val t = interpolator.getInterpolation(elapsed.toFloat() / duration)

                    val rot = t * toRotation + (1 - t) * startRotation

                    val bearing = if (-rot > 180) rot / 2 else rot

                    marker.rotation = bearing

                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16)
                    } else {
                        isMarkerRotating = false
                    }
                }
            })
        }
    }

    fun animateMarker(marker: Marker, toPosition: LatLng,
                      hideMarker: Boolean) {
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val proj = mMap!!.getProjection()
        val startPoint = proj.toScreenLocation(marker.position)
        val startLatLng = proj.fromScreenLocation(startPoint)
        val duration: Long = 500


        val interpolator = LinearInterpolator()

        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                val lng = t * toPosition.longitude + (1 - t) * startLatLng.longitude
                val lat = t * toPosition.latitude + (1 - t) * startLatLng.latitude
                val newPosition = LatLng(lat, lng)
                marker.setPosition(newPosition)
                polyline.add(newPosition)
                polyline.width(10f)
                polyline.color(Color.BLUE)
                mMap!!.addPolyline(polyline)

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                } else {
                    if (hideMarker) {
                        marker.isVisible = false
                    } else {
                        marker.isVisible = true
                    }
                }
            }
        })
    }

    fun trazarRuta(directions: PlacesDirections) {
        try{
            //mMap!!.setOnCameraIdleListener(null)
            isMapSelection = false
            Log.v("MapReady", "Trazando Ruta")
            Log.v("MapReady", "Cambiando Vistas")
//            collapse(selectLocationsContainer!!)
//            collapse(predictionContainer!!)
            selectLocation!!.visibility = View.GONE
            btnFinish!!.visibility = View.GONE
////            predictionContainer!!.visibility = View.GONE
//            mapContainer!!.visibility = View.VISIBLE
            var duration = -1


//            expand(confirmServiceContainer!!)
            val points = ArrayList<LatLng>()
            var lineOptions: PolylineOptions? = null
            val markerOptions = MarkerOptions()
            var routes: List<Route> = ArrayList<Route>()
            var legs: List<Leg> = ArrayList<Leg>()
            var steps: List<Step> = ArrayList<Step>()
            var htmlInstructions: String? = null

            if (directions.getRoutes().isNotEmpty()) {
                routes = directions.getRoutes()
                if (routes[0].getLegs() != null && routes[0].getLegs().isNotEmpty()) {
                    legs = routes[0].getLegs()
                    for (item in legs) {
                        try{
                            var getDuration = item!!.duration.text.split(" ")
                            duration = getDuration[0].toInt()
                        }catch (e:Exception){
                            Log.e("DurationException",e.toString())
                        }

                        if (item.getSteps() != null && item.getSteps().isNotEmpty()) {
                            steps = item.getSteps()
                            val aTagRegex = "<[^>]*>"
                            htmlInstructions = steps[0].htmlInstructions
                            htmlInstructions = htmlInstructions.replace(aTagRegex.toRegex(),"")
                            try{
                                htmlInstructions = htmlInstructions.replace("Vía de uso restringido","")
                            }catch(e:Exception){
                                Log.e("ReplaceException",e.toString())
                            }

                            if (htmlInstructions!=null){
                                directionsContainer!!.visibility = View.VISIBLE
                                txtDirections!!.setText(htmlInstructions)
                            }
                            Log.v("HtmlInstructions", htmlInstructions)
                            for (item2 in steps) {
                                Log.v("DirectionPolyline", item2.getPolyline().getPoints())
                                lineOptions = PolylineOptions()
                                val latInfo = item2.getStartLocation().getLat().split(",")
                                val lat = java.lang.Double.parseDouble(latInfo[0])
                                val lng = java.lang.Double.parseDouble(item2.getStartLocation().getLng())
                                val position = LatLng(lat, lng)
                                val paths = decodePoly(item2.getPolyline().getPoints())
                                points.addAll(paths)
                            }
                        }
                    }
                }
                // Adding all the points in the route to LineOptions
                lineOptions!!.addAll(points)
                lineOptions.width(10f)
                lineOptions.color(R.color.colorPrimary)

                // Drawing polyline in the Google Map for the i-th route
                var destinyPosition: MutableList<String>? = null
                var destinyPositionNext: MutableList<String>? = null
                var destinyPositionNextSecond: MutableList<String>? = null
                var nextLatLng : LatLng? = null
                if (points != null && points.size > 0) {
                    destinyPosition = ArrayList<String>()
                    destinyPosition.add(points[points.size - 1].latitude.toString())
                    destinyPosition.add(points[points.size - 1].longitude.toString())
                    try {
                        destinyPositionNext = ArrayList<String>()
                        destinyPositionNext.add(points[0].latitude.toString())
                        destinyPositionNext.add(points[0].longitude.toString())
                    } catch (e: Exception) {
                        Log.v("LocationException", e.toString())
                    }

                    try {
                        destinyPositionNextSecond = ArrayList<String>()
                        destinyPositionNextSecond.add(points[1].latitude.toString())
                        destinyPositionNextSecond.add(points[1].longitude.toString())
                    } catch (e: Exception) {
                        Log.v("LocationException", e.toString())
                    }

                    Log.v("LocationChange", "Latitude: " + destinyPosition[0])
                    Log.v("LocationChange", "Longitude: " + destinyPosition[1])
                    //                destinyPosition = destino.split(",");
                }
//                if (destino != "")
//                    destinyPosition = destino!!.split(",")
                if (destinyPosition != null) {
                    if (destinyPositionNext != null && destinyPositionNext.size > 0)
                        if (destinyPositionNext[0] != null && destinyPositionNext[1] != null)
                            lastLatLng = LatLng(java.lang.Double.parseDouble(destinyPositionNext[0]), java.lang.Double.parseDouble(destinyPositionNext[1]))
                    if (destinyPositionNextSecond != null && destinyPositionNextSecond.size > 0)
                        if (destinyPositionNextSecond!![0] != null && destinyPositionNextSecond[1] != null)
                            nextLatLng = LatLng(java.lang.Double.parseDouble(destinyPositionNextSecond[0]), java.lang.Double.parseDouble(destinyPositionNextSecond[1]))
                    val destinoLatLng = LatLng(java.lang.Double.parseDouble(destinyPosition[0]), java.lang.Double.parseDouble(destinyPosition[1]))

                    if (this.poly != null) {
                        this.poly!!.remove()
                    }
                    this.poly = mMap!!.addPolyline(lineOptions)

                    if (this.lastLatLng != null) {
                        this.lastBearing = bearingBetweenLocations(this.lastLatLng!!, destinoLatLng).toFloat()
//                        rotateMarker(this.marker!!, this.lastBearing)
                        animateMarker(marker!!, lastLatLng!!, nextLatLng!!, false)
                    }
//                    mMap!!.addMarker(MarkerOptions()
//                            .position(origenLatLng)
//                            .icon(BitmapDescriptorFactory.fromBitmap(iconOrigen))
//                            .infoWindowAnchor(0F,0F)
//                            .flat(true)).showInfoWindow()
                    val builder = LatLngBounds.Builder()
                    for (latLng in points) {
                        builder.include(latLng)
                    }
                    val bounds = builder.build()
                    //BOUND_PADDING is an int to specify padding of bound.. try 100.
                    val width = getResources().getDisplayMetrics().widthPixels
                    val height = (getResources().getDisplayMetrics().heightPixels * 0.75).toInt()
                    val minMetric = Math.min(width, height)
                    val padding = (minMetric * 0.30).toInt()
                    Log.v("BoundPadding","Width: "+width.toString())
                    Log.v("BoundPadding","Height: "+height.toString())
                    Log.v("BoundPadding","MinMetric: "+padding.toString())
                    Log.v("BoundPadding","Padding: "+padding.toString())
//                    val cu = CameraUpdateFactory.newLatLngBounds(bounds,width,height, padding)
                    val cu = CameraUpdateFactory.newLatLngZoom(lastLatLng,18.0f)
                    mMap!!.animateCamera(cu)
//                    mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(newLocation, 16.0f))
                }

            }
        }catch (e:Exception){
            Toast.makeText(this@MenuActivity,"Exception: "+e.toString(),Toast.LENGTH_LONG).show()
        }
    }

    private fun bearingBetweenLocations(paramLatLng1: LatLng, paramLatLng2: LatLng): Double {
        var d1 = paramLatLng1.latitude * 3.14159 / 180.0
        var d3 = paramLatLng1.longitude * 3.14159 / 180.0
        val d2 = paramLatLng2.latitude * 3.14159 / 180.0
        d3 = paramLatLng2.longitude * 3.14159 / 180.0 - d3
        d1 = (360.0 + Math.toDegrees(Math.atan2(Math.sin(d3) * Math.cos(d2), Math.cos(d1) * Math.sin(d2) - Math.sin(d1) * Math.cos(d2) * Math.cos(d3)))) % 360.0
        Log.v("MarkerRotate", "" + d1)
        return d1
    }

    private fun decodePoly(encoded: String): List<LatLng> {

        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val p = LatLng(lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5)
            poly.add(p)
        }
        return poly
    }

    private fun getDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        //double earthRadius = 3958.75;//miles
        val earthRadius = 6371.0//kilometers
        //        double earthRadius = 6371000;//metros
        val dLat = Math.toRadians(lat2 - lat1)
        val dLng = Math.toRadians(lon2 - lon1)
        val sindLat = Math.sin(dLat / 2)
        val sindLng = Math.sin(dLng / 2)
        val a = Math.pow(sindLat, 2.0) + Math.pow(sindLng, 2.0) *
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        return earthRadius * c
    }

//    fun start(context: Context, type:String, socialUser: SocialUser){
//        var intent = Intent(context, MenuActivity::class.java)
//        intent.putExtra(EXTRA_USER, socialUser)
//        intent.putExtra(EXTRA_TYPE, type)
//        context.startActivity(intent)
//    }

    override fun onResume() {
        super.onResume()
        setUserData()
        sensorMan!!.registerListener(this, accelerometer,
                SensorManager.SENSOR_DELAY_UI)
    }

    fun revealMap(newLocation: LatLng){
        var x = revealContainer!!.getRight()
        var y = revealContainer!!.getBottom()

//        var startRadius = 0
//        var endRadius =  Math.hypot(revealContainer!!.getWidth().toDouble(), revealContainer!!.getHeight().toDouble()).toInt()

        var startRadius = Math.max(revealContainer!!.getWidth().toDouble(), revealContainer!!.getHeight().toDouble()).toInt()
        var endRadius =  0

        // get the center for the clipping circle
        var cx = revealContainer!!.getMeasuredWidth() / 2
        var cy = revealContainer!!.getMeasuredHeight() / 2

        // get the initial radius for the clipping circle
        var initialRadius = revealContainer!!.getWidth() / 2

        var anim = ViewAnimationUtils.createCircularReveal(revealContainer, cx, cy, initialRadius.toFloat(),0F)
        anim.duration = 1000
//        anim.startDelay = 1500

        anim.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {
            }

            override fun onAnimationEnd(animation: Animator?) {
                Log.e("Animator","Animation Ends")
                mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(newLocation, 16.0f))
                revealContainer!!.setVisibility(View.GONE)
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationStart(animation: Animator?) {
                Log.e("Animator","Animation Starts")
            }
        })
//        revealContainer!!.setVisibility(View.GONE)
        anim.start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE){
            if (resultCode == Activity.RESULT_OK){
                finish()
                var intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
            }
        }
    }

    fun selectLocationInMap(type:Int){
        if (mMap!=null){
            isMapSelection = true
            directionsContainer!!.visibility = View.GONE
            txtDirections!!.setText("")
            mMap!!.clear()
            val iconCar = BitmapFactory.decodeResource(this@MenuActivity.getResources(),
                    R.mipmap.car_small)
            marker = mMap!!.addMarker(MarkerOptions()
                    .position(myLocation!!)
                    .icon(BitmapDescriptorFactory.fromBitmap(iconCar))
                    .flat(true))
            selectLocation!!.visibility = View.VISIBLE
            if (currentLocation!=null){
                var latLng = LatLng(currentLocation!!.latitude,currentLocation!!.longitude)
                mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f))
            }
        }
    }

    fun movingMap(){
        btnFinish!!.visibility = View.GONE
    }

    fun getLocationFromMap(){
        if (isMapSelection){
            Log.e("MapMove","Locating current tarjet")
            //btnFinish!!.visibility = View.GONE
            val midLatLng = mMap!!.getCameraPosition().target
            val placeDetailsApi = PlaceGeocodeApi(this@MenuActivity, ORIGEN)
            placeDetailsApi.getPlaceGeocode(midLatLng)
        }
    }

    fun setLocationDetails(result: com.aarr.vip365driver.services.PlaceGeocode.Result, estado: Int) {
        Log.e("MapMove","Set Location Details")
        var direccion: String? = ""
        val lat = result!!.getGeometry().getLocation().getLat()
        val lng = result!!.getGeometry().getLocation().getLng()
        val location = Location(android.location.LocationManager.GPS_PROVIDER)
        location.latitude = lat.toDouble()
        location.longitude = lng.toDouble()
        destino = location.latitude.toString()+","+location.longitude.toString()
        if (destino!=null){
            btnFinish!!.visibility = View.VISIBLE
        }
    }

}
