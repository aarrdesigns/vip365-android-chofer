package com.aarr.vip365driver.services.request

import java.io.Serializable

/**
 * Created by andresrodriguez on 10/17/17.
 */
data class UpdateDriverLocationRequest(
        var user_driver_id:Int,
        var driver_location:String
): Serializable {
    constructor():this(
            -1,
            ""
    )
}