package com.aarr.vip365driver.services

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.aarr.vip365driver.database.Dao.UsersDao
import com.aarr.vip365driver.database.Model.UsersModel
import com.aarr.vip365driver.services.request.LoginRequest
import com.aarr.vip365driver.services.request.UpdateDriverLocationRequest
import com.aarr.vip365driver.services.response.InsertResponse
import com.aarr.vip365driver.services.response.LoginResponse
import com.aarr.vip365driver.views.LoginActivity
import com.aarr.vip365driver.views.MenuActivity
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by andresrodriguez on 10/17/17.
 */
class UpdateDriverLocationService {
    private var context: Context? = null
    private var service: ApiRest? = null
    private var activity: MenuActivity? = null

    constructor(context: Context){
        this.context = context
        if (context is LoginActivity){
            activity = context as MenuActivity
        }
    }

    fun handleUpdate(newLocation:String){
//        val gson = GsonBuilder()
//                .setLenient()
//                .create()

        val retrofit = Retrofit.Builder()
                .baseUrl("http://vip365.000webhostapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        Log.v("Service", "Url:" + retrofit.baseUrl().toString())
        service = retrofit.create(ApiRest::class.java)

        var user = UsersDao().getCurrentUser()
            var request = UpdateDriverLocationRequest(
                    user!!.idServer,
                    newLocation
            )

            try {
//                var response: Call<LoginResponse>? = service!!.login(request)
                var response: Call<InsertResponse>? = service!!.updateDriverLocation(
                        request.user_driver_id,
                        request.driver_location
                )
                response!!.enqueue(object : Callback<InsertResponse> {

                    override fun onResponse(call: Call<InsertResponse>?, response: retrofit2.Response<InsertResponse>?) {
                        Log.v("Service", "Success")
                        Log.v("Service", "Response: " + response!!.raw().toString())
                        Log.v("Service", "Response: " + response!!.message())
                        for (i in response.headers().names()) {
                            Log.v("Service", "Header: " + i + ": " + response!!.headers().get(i))
//                            Log.v("Service","ResponseHeader: "+i)
                        }
//                        Log.v("Service","Data:"+response!!.body().toString())
                        var responseLogin = response!!.body()
                        if (responseLogin != null) {
                            Log.v("Service", "Data: " + responseLogin.toString())
                            if (responseLogin!!.id != null && responseLogin!!.id!!>0) {
                                Log.v("Service", "USER IS NOT NULL")

                            } else {
                                Log.v("Service", "USER IS NULL")
//                                activity!!.dismissDialog()
                                if (responseLogin.msgError != null) {
                                    Toast.makeText(context, responseLogin.msgError, Toast.LENGTH_LONG).show()
                                } else {
                                    Toast.makeText(context, "Error al iniciar sesion", Toast.LENGTH_LONG).show()
                                }
                            }
                        }else{
//                            activity!!.dismissDialog()
                            Toast.makeText(context, "Error de servidor", Toast.LENGTH_LONG).show()
                        }
                    }
                    override fun onFailure(call: Call<InsertResponse>?, t: Throwable?) {
//                        activity!!.dismissDialog()
                        Log.v("Service", "Failed:" + t.toString())
                    }
                })

            } catch (e: JSONException) {
//                activity!!.dismissDialog()
                Log.v("Service", "Exception:" + e.toString())
            }
    }
}