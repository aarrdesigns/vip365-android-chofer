package com.aarr.vip365driver.services

import com.aarr.vip365driver.services.PlaceAutocomplete.PlaceAutocomplete
import com.aarr.vip365driver.services.PlaceDetails.PlaceDetails
import com.aarr.vip365driver.services.PlaceDetails.PlaceDetailsByLocation
import com.aarr.vip365driver.services.PlaceDirections.PlacesDirections
import com.aarr.vip365driver.services.PlaceGeocode.PlaceGeocode

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * Created by andresrodriguez on 1/19/17.
 */
interface MapsServices {

    @GET
    fun getPlacesAutocomplete(@Url url:String): Call<PlaceAutocomplete>

    @GET
    fun getPlaceDirections(@Url url:String): Call<PlacesDirections>

    @GET
     fun getPlaceDetails(@Url url: String): Call<PlaceDetails>

    @GET
     fun getPlaceDetailsByLocation(@Url url: String): Call<PlaceDetailsByLocation>

    @GET
     fun getPlaceGeocode(@Url url: String): Call<PlaceGeocode>

//    @POST("get-all-categories.php")
//    fun GetAllCategories(): Call<CategoriesResponse>
}
