package com.aarr.vip365driver.services.request

import java.io.Serializable

/**
 * Created by andresrodriguez on 10/17/17.
 */
data class PaymentMethodsRequest(
        var id:Int,
        var id_user:Int,
        var card_type:String,
        var card_number:String,
        var card_security_number:String,
        var card_holder_name:String,
        var payment_type:Int,
        var paypal_code:String,
        var is_default:Boolean,
        var is_active:Boolean
): Serializable {
    constructor():this(
            -1,
            -1,
            "",
            "",
            "",
            "",
            -1,
            "",
            false,
            false
    )
}