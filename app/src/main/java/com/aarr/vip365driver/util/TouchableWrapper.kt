package com.aarr.vip365.util

import android.content.Context
import android.util.Log
import android.view.MotionEvent
import android.widget.FrameLayout
import com.aarr.vip365driver.views.MenuActivity


/**
 * Created by andresrodriguez on 10/31/17.
 */
class TouchableWrapper(context: Context) : FrameLayout(context) {

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {

        var activity: MenuActivity? = null
        if (context is MenuActivity){
            activity = context as MenuActivity
        }
        when (event.action) {

            MotionEvent.ACTION_DOWN -> {
                Log.e("MapMove","Action DOWN")
                if (activity!=null){
                    activity.movingMap()
                }
            }

            MotionEvent.ACTION_UP -> {
                Log.e("MapMove","Action UP")
                if (activity!=null){
                    activity.getLocationFromMap()
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }
}