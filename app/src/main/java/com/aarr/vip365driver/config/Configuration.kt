package com.aarr.vip365driver.config

/**
 * Created by jrobe on 24/02/2017.
 */
class Configuration {

    val dbName = "/data/data/com.aarr.vip365driver/vip365Driver_db.s3db"
    val PAYMENT_CREDIT_DEBIT_CARD = 1
    val PAYMENT_PAYPAL = 2
}
